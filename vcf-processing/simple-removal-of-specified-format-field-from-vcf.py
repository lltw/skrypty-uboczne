#!/usr/bin/env python3

import argparse
from sys import stdin
from signal import signal, SIGPIPE, SIG_DFL

signal(SIGPIPE, SIG_DFL)

parser = argparse.ArgumentParser(formatter_class=argparse.RawDescriptionHelpFormatter,
                                 description=
"""
Simple removal of specified FORMAT field from VCF.
""")

parser.add_argument('FIELD_NAME', type=str, help="Name of FORMAT field to remove from VCF.")

args = parser.parse_args()

field_name = args.FIELD_NAME

args = parser.parse_args()

for raw_line in stdin:

    if raw_line.startswith('##FORMAT=<ID=' + field_name + ','):
        pass
    elif raw_line.startswith('#'):
        print(raw_line.strip())
    else:

        line = raw_line.strip().split("\t")
        FORMAT = line[8].replace(":" + field_name, "")

        try:
            index_of_field = line[8].split(":").index(field_name)
        except:
            index_of_field = -1

        if index_of_field > -1:
            GENOTYPES = [":".join(x.split(":")[:index_of_field] + x.split(":")[index_of_field + 1:]) for x in line[9:]]
            print("\t".join(line[0:8] + [FORMAT] + GENOTYPES).strip())
        else:
            print(raw_line.strip())





