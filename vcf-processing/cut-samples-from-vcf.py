#!/usr/bin/env python3


import argparse
from sys import stdin, stderr
from signal import signal, SIGPIPE, SIG_DFL
signal(SIGPIPE, SIG_DFL)

parser = argparse.ArgumentParser(formatter_class=argparse.RawDescriptionHelpFormatter,
                                 description=
"""

Print VCF containing genotypes only for specified samples or for all samples except those specified.

""")

parser.add_argument('SAMPLES_LIST', type=str, help="Comma-separated list of samples IDs")
parser.add_argument('-f', action='store_true', default=False, help="Provide list of samples IDs in a file rather that a comma-separated list of samples IDs (one sample ID per line) (Default: cut all the samples except selected)")
parser.add_argument('-r', action='store_true', default=False, help="Cut out selected samples (Default: cut all the samples except selected)")
parser.add_argument('-x', action='store_true', default=False, help="Do not exclude lines with no alternative genotype (Default: print only lines with at least one non-reference allele")

args = parser.parse_args()

samples_list = args.SAMPLES_LIST
reverse = args.r
do_not_exclude = args.x


def get_samples_list(samples_list_file):

    samples_list = []

    for line in open(samples_list_file, 'r'):
        samples_list.append(line.strip())

    return samples_list


def get_samples_indices(header_line, samples_list, reverse):

    all_samples_list = header_line.strip().split('\t')

    indices = [x for x in range(0, 9)]

    if reverse:

        all_samples_list_only = header_line.strip().split('\t')[9:]

        for sample in all_samples_list_only:

            if sample not in samples_list:
                indices.append(all_samples_list.index(sample))

    else:

        for sample in samples_list:

            if sample in all_samples_list:
                indices.append(all_samples_list.index(sample))
            else:
                print('Sample: ' + sample + ' is not in the VCF', file=stderr)

    return indices


def get_line_if_at_least_one_nonreference_genotype(line, indices):

    line = line.strip().split('\t')

    genotypes = []

    for genotype in [line[i] for i in indices[9:]]:
        genotypes.append(genotype.strip().split(':')[0])

    genotypes = set(genotypes) - {'./.', '0/0'}

    if len(genotypes) != 0:
        return '\t'.join([line[i] for i in indices])
    else:
        return None


def get_line(line, indices):

    line = line.strip().split('\t')

    genotypes = []

    for genotype in [line[i] for i in indices[9:]]:
        genotypes.append(genotype.strip().split(':')[0])

    return '\t'.join([line[i] for i in indices])



if (args.f):
    samples_list = get_samples_list(samples_list)
else:
    samples_list = samples_list.split(',')

for line in stdin:

        if line.startswith('##'):
            print(line.strip())
        elif line.startswith('#C'):
            indices = get_samples_indices(line, samples_list, reverse)
            line = line.strip().split('\t')
            print('\t'.join([line[i] for i in indices]))
          
        else:

            if (do_not_exclude):
                new_line = get_line(line, indices)
            else:
                new_line = get_line_if_at_least_one_nonreference_genotype(line, indices)

            if new_line:
                print(new_line)

