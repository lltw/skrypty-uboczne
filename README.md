## Table of Contents

<!---toc start-->

* **VCF processing**
  * [Cut samples from VCF](#cut-samples-from-vcf)
  * [Simple removal of specified format field from VCF](#simple-removal-of-specified-format-field-from-vcf)

<!---toc end-->

---


### Cut samples from VCF

**Author**:  Katarzyna Kolanek


```
usage: cut-samples-from-vcf.py [-h] [-f] [-r] [-x] SAMPLES_LIST

Print VCF containing genotypes only for specified samples or for all samples except those specified.

positional arguments:
  SAMPLES_LIST  Comma-separated list of samples IDs

optional arguments:
  -h, --help    show this help message and exit
  -f            Provide list of samples IDs in a file rather that a comma-
                separated list of samples IDs (one sample ID per line)
                (Default: cut all the samples except selected)
  -r            Cut out selected samples (Default: cut all the samples except
                selected)
  -x            Do not exclude lines with no alternative genotype (Default:
                print only lines with at least one non-reference allele
```

### Simple removal of specified format field from VCF

**Author**:  Katarzyna Kolanek

```
usage: simple-removal-of-specified-format-field-from-vcf.py [-h] FIELD_NAME

Simple removal of specified FORMAT field from VCF.

positional arguments:
  FIELD_NAME  Name of FORMAT field to remove from VCF.

optional arguments:
  -h, --help  show this help message and exit
```

